/**
 * CuForms - Custom Form Element Plugin - Outdated - Not Working!!!
 */

//Extending jQuery
jQuery.extend({

    //cuForms all functions
    cuForms : {

        //Main create function
        create : function (elems, opts) {

            //Reference to cuForms, passed options, all elements to create custom
            var elements    = jQuery(elems),
                cuForms     = this,
                options     = jQuery.extend(this.options, opts);

            //Add element and option set to array so refresh() can be done
            options.elementArraySet.push({elems : elems, opts : opts});

            //No elements selected, return
            if (!elements.length) {
                return;
            }

            //Some elements are selected, continue
            elements.each(function () {

                //Reference to each element
                var element = jQuery(this),
                    elementParent = element.parent();

                //If element is already changed - return. Hidden - will manage on event()
                if (element.is(':hidden') || element.hasClass(options.customElementClass)) {
                    return;
                }

                //If element is simple select, radio or checkbox continue
                if ((!element.attr('multiple') && element.is('select')) || element.is(':checkbox') || element.is(':radio')) {

                    //If in table, wrap in into div, so position relative can be used
                    if (elementParent.is('td') || elementParent.is('th')) {
                        element.wrap('<span/>');
                    }

                    //Vars for class name and status
                    var tagName = '',
                        status = '';

                    element.addClass(options.customElementClass).css('opacity', 0);

                    //Checking element's statuses and adding classes if needed
                    if (element.is(':radio')) {
                        tagName = options.customRadioClass + ' ';
                    }
                    if (element.is(':checkbox')) {
                        tagName = options.customCheckboxClass + ' ';
                    }
                    if (element.is('select')) {
                        tagName = options.customSelectClass + ' ';
                    }
                    if (element.attr('checked')) {
                        status += options.activeElementClass + ' ';
                    }
                    if (element.attr('disabled')) {
                        status += options.disabledElementClass + ' ';
                    }

                    //Fix Webkit for MAC select height
                    if (navigator.appVersion.indexOf("Mac") !== -1 && element.is('select')) {
                        element.css({'-webkit-appearance' : 'menulist-button'});
                    }

                    //New element and attributes for it
                    var parent      = element.parent().addClass(options.customParentClass),
                        position    = element.position(),
                        width       = element.outerWidth(),
                        height      = element.outerHeight(),
                        custom      = jQuery('<div/>', {
                            'class'   : options.bckElementClass + ' ' + tagName + status + options.addCustomClass,
                            'css'     : {
                                'top'               : position.top,
                                'left'              : position.left,
                                'margin-top'        : element.css('margin-top'),
                                'margin-right'      : element.css('margin-right'),
                                'margin-bottom'     : element.css('margin-bottom'),
                                'margin-left'       : element.css('margin-left'),
                                'width'             : width,
                                'height'            : height
                            }
                        });

                    //If it's select we will need two more elements
                    if (element.is('select')) {

                        //Outer element - sliding doors right side
                        var selectOuter = jQuery('<div/>', {
                            'class'   : options.selectOuterElemClass,
                            'css'     : {
                                'width'             : width - options.selectPadding,
                                'height'            : height,
                                'padding-right'     : options.selectPadding
                            }
                        });

                        //Inner element - contains text, left side of sliding door
                        var selectInner = jQuery('<div/>', {
                            'class'   : options.selectInnerElemClass,
                            'css'     : {
                                'width'             : '100%',
                                'height'            : height
                            }
                        });

                        //Add current option's text and add to custom element
                        selectInner.text(element.find('option:selected').text());
                        custom.append(selectOuter.append(selectInner));
                    }

                    //Add new element to DOM
                    custom.insertBefore(element);

                    //Mouseover and mouseout events for .hover class
                    element.mouseover(function () {
                        element.prev().addClass(options.hoverElementClass);
                    }).mouseleave(function () {
                            element.prev().removeClass(options.hoverElementClass);
                        });
                }
            });

            //Add listeners to all potential elements that may change radios, checkboxes or selects
            if (!options.eventAttached) {

                //Add listener to elements, if occurs - event() function
                jQuery(options.potentialClick).live('click', function () { cuForms.event(); });
                jQuery(options.potentialChange).live('change', function () { cuForms.event(); });

                //No need to attach it again and again
                options.eventAttached = true;
            }
        },
        //Function to check custom form element statuses
        event : function () {

            //Reference to cuForms
            var cuForms = this;

            //Do the refresh, simply does nothing on already changed elements and changes all new, previously hidden
            cuForms.refresh();

            //Loop through all elements that have custom element
            jQuery('.' + cuForms.options.customElementClass).each(function () {

                //Vars that will be used
                var element = jQuery(this),
                    custom = element.prev();

                //Checked
                if (!element.attr('checked')) {
                    custom.removeClass(cuForms.options.activeElementClass);
                } else {
                    custom.addClass(cuForms.options.activeElementClass);
                }

                //Disabled
                if (!element.attr('disabled')) {
                    custom.removeClass(cuForms.options.disabledElementClass);
                } else {
                    custom.addClass(cuForms.options.disabledElementClass);
                }

                //Hidden
                if (element.css('display') === 'none') {
                    custom.addClass(cuForms.options.hiddenElementClass);
                } else {
                    custom.removeClass(cuForms.options.hiddenElementClass);
                }

                //If select, change text
                if (element.is('select')) {
                    custom.find('.' + cuForms.options.selectInnerElemClass).text(element.find('option:selected').text());
                }
            });
        },
        //Refresh function
        refresh : function () {

            //Reference to cuForms
            var cuForms = this;

            //Clear element array, because create will add them
            var elementArraySet = cuForms.options.elementArraySet;
            cuForms.options.elementArraySet = [];

            //Fire create functions
            jQuery.each(elementArraySet, function (i) {
                cuForms.create(elementArraySet[i].elems, elementArraySet[i].opts);
            });
        },
        //Destroy function to remove custom element and clear stored elementArraySet if needed
        destroy : function (leaveArrays) {

            //Reference to cuForms
            var cuForms = this;

            //Loop through each in array
            jQuery('.' + cuForms.options.customElementClass).each(function () {

                //Vars that will be used
                var element = jQuery(this),
                    custom = element.prev();

                //Remove classes and custom element, reset opacity
                element.removeClass(cuForms.options.customElementClass).css('opacity', 1);
                element.parent().removeClass(cuForms.options.customParentClass);
                custom.remove();

                //Remove attached events to elements and css
                element.unbind('mouseover').unbind('mouseout');
                element.css({'-webkit-appearance' : ''});

                //Remove attached events to global elements
                jQuery(cuForms.options.potentialClick).die('click');
                jQuery(cuForms.options.potentialChange).die('change');

                //Clear element arrays if needed
                if (!leaveArrays) {
                    cuForms.options.elementArraySet = [];
                }

                //Make sure live clicks, changes on potential elements are added again on create
                cuForms.options.eventAttached = false;
            });
        },
        //Options for cuForms
        options : {
            //CSS classes - if changed, changes in cuforms.css are required
            customParentClass    :   'cuForms_customParent',
            customElementClass   :   'cuForms_customElement',
            bckElementClass      :   'cuForms_customOver',
            customRadioClass     :   'cuForms_radio',
            customCheckboxClass  :   'cuForms_checkbox',
            customSelectClass    :   'cuForms_select',
            hiddenElementClass   :   'cuForms_hidden',
            hoverElementClass    :   'cuForms_hover',
            activeElementClass   :   'cuForms_active',
            disabledElementClass :   'cuForms_disabled',
            selectInnerElemClass :   'cuForms_selectInner',
            selectOuterElemClass :   'cuForms_selectOuter',
            //Passing options
            addCustomClass       :   '',
            selectPadding        :   28,
            //Element array
            elementArraySet      :   [],
            //Potential click/change elements
            potentialClick       :   'input:radio, input:checkbox, *[onclick], button',
            potentialChange      :   'select'
        }
    }
});